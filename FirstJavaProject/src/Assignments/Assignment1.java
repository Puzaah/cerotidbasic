package Assignments;

public class Assignment1 {

	public static void main(String[] args) {

		// Create a String[] Array of your Favorite Celebrity's (Size 10)

		String[] Celeb = { "Emilia Clarke", "Maisie Williams", "Dayahang Rai", "Aamir Khan", "Eminem",
				"Gabriel Iglesias", "Bibek Waiba lama", "Rajnikanth", "Travis Scott", "Priyanka Chopra" };

		// Create a loop that iterates through the size of the Celebrity's

		for (int i = 0; i < Celeb.length; i++) {
			System.out.println(Celeb[i]);
			
			// . Create a if condition within the loop that checks if your particular
			// Celebrity exits.
			// (celebs[i].contains("your celeb"))

			if (Celeb[i].contains("Bibek Waiba lama")) {
				System.out.println("Your favourite Celeb " + Celeb[i] + " found.");
			}

			// write a print statement inside your if condition that asks a question to your
			// Celebrity
			// and print in to the console.

			if (Celeb[i].equals("Eminem")) {
				System.out.println("Can you please wish my sister on her birthday?");

			}

		}

	}
}
