package Assignments;

import java.util.Scanner;

public class Assignment2 {

	// creating a method to calculate average score
	static double gradeCalculator(double sum, int numOfCourses) {

		double classAverage = sum / numOfCourses;
		System.out.println("Average score is :" + classAverage);
		return classAverage;

	}

	// main method
	public static void main(String[] args) {

		// Creating Variables
		int numOfCourses = 5;
		double sum = 0;
		double classAverage = 0;
		double input;

		// Creating a Scanner Object to read user input
		Scanner scanner = new Scanner(System.in);

		// Creating an Array to store 5 courses
		int courses[] = new int[5];

		// for loop that iterates through the the length of the courses array
		for (int i = 0; i < courses.length; i++) {

			// do while to keep input in range
			do {

				System.out.println("Please Enter Course " + (i + 1) + " Grade(numbers only): ");

				// read the given input and add up sum
				input = scanner.nextDouble();
			} while (input > 100 || input < 0);

			sum = sum + input;
		}

		// calling method
		classAverage = gradeCalculator(sum, numOfCourses);

		if (classAverage < 60) {
			System.out.println("Average Grade: F"); 
		} else if (classAverage <= 69) {
			System.out.println("Average Grade: D");
		} else if (classAverage <= 79) {
			System.out.println("Average Grade: C");
		} else if (classAverage <= 89) {
			System.out.println("Average Grade: B");
		} else if (classAverage <= 100) {
			System.out.println("Average Grade: A");
		}
		// closing scanner
		scanner.close();

	}
}
