package com.cerotid.basic;

public class ClassesAndObjects {
	// class level variables
	int students = 24;
	String name = "QA";
	public static void main(String[] args) {
		
		ClassesAndObjects myClass = new ClassesAndObjects();
		System.out.println(myClass.students);
		System.out.println(myClass.name);
		
		Java myJavaClass = new Java(); // object 1
		cPlusPlus mycPlusPlus = new cPlusPlus(); //object 2
		
		//printing values for two seperate classes
		
		System.out.println(myJavaClass.isOOPS);
		System.out.println(mycPlusPlus.isOOPS);
		
		
	}
}	

class Java{
	boolean isOOPS = true;
	
}
class cPlusPlus{
	boolean isOOPS =true;
	
}

	


