package com.cerotid.basic;

public class DoWhile {
	public static void main(String[] args) {
		/*
		 * do{
		 * 
		 * 
		 * }while(condition);
		 */
		//program to show do while loop
		int x = 21;
		
		do {
			//line will printed even
			System.out.println("Value of x: "+ x);
			x++;
		}while (x<30);
			
	  }
		
		
	}


