package com.cerotid.basic;

public class ForLoop {
	public static void main(String[] args) {
		/*
		 * for(initilazation condition; testing condition{
		 * increment/decrement}
		 * statements
		 */
		
		
		for (int i = 0; i < 20; i++) {
			//for loop begins when x=5 and runs till x<=20
			for (int x = 5; x < 20; x++) {
				System.out.println("Value of x:" + x);
				
			}
			
		}
		
	}

}
