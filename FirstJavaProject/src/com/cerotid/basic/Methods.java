package com.cerotid.basic;

public class Methods {
	
	// methods is a block of code that runs only when called
	// you can pass different data- known as parameters
	// methods are used to perform certain actions and these action are known as
	// functions
	public static void main(String[] args) {
		
		//calling methods
		PrintExecuting();
	}

	// creating a method

	static void myMethod() {
		System.out.println("I am in Java/QA Class");
		// code or logic goes here
	}

	static void PrintExecuting() {

		System.out.println("I just got executed");
	}
	
	static void classwork() {
		System.out.println("This is my first classwork ");
	}
}

class car {
	// num of doors
	int numOfDoors;
	String Color;
	String bodytype;

}
