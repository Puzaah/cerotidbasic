package com.cerotid.basic;


public class MethodsWithParameters {
	public static void main(String[] args) {
		//made methods below and calling them here
		
		//1st method
		myName("Ashwin ");
		//2nd method
		add(5,5);
		//3rd method
		nameAdd(2,2,"Puja");
		//4th method
		nameAddBetter();
		//5th method -- return type
		returnInt(5);
		System.out.println(returnInt(5));
		//6th method -- return type with parameters
		 multiply(6,6);
		 System.out.println(multiply(6,6));
		 //7th method
		 System.out.println(subtraction(10,2));
		 //8th method
		 System.out.println(division(100,50));
		 
		 
		 
		 
		 
		
		
	}
	static void myName(String name) {
		System.out.println(name + "is in Java/QA Class");
		
	}
	static void add(int i,int x) {
		int sum = i + x;
		System.out.println(sum);
	}
	static void nameAdd(int a,int b,String name) {
		int sum =a+b;
		System.out.println("Hello "+ name);
		System.out.println(sum);
	}
	
	static void nameAddBetter() {
		myName(" Puja");
		add(90,10);
		
	}
	
	//void : a void method has no return type
	static int returnInt(int x) {
		
		x=x*5;
		return x;
	}
	
	static int multiply(int x,int y) {
		int z =x*y;
		return z;
	}
	static int subtraction(int x,int y) {
		int z = x-y;
		return z;
	}
	static int division(int x,int y) {
		
		return x/y;
	}
	
	

}
