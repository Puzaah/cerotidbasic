package com.cerotid.basic;

public class MultiDimentionalArray {
	/*
	 * syntax of multidimentional arrays datatype[1st demention][2nd demention][3rd
	 * dimention] arrayname=new data_type[size1][size2][size3];
	 */

	/*
	 * two Dimentional array example String[][] twoDimentionalArray = new
	 * String[10][20]; int[][]twoDimentionalArrayInteger = new int[10][20];
	 * 
	 * //three Dimentional arrays example
	 * 
	 * int[][][] threeD_Array = new int[10][20][30];
	 */
	// 2D example
	public static void main(String[] args) {
		int[][] arr1 = new int[10][20];

		arr1[0][0] = 1;
		System.out.println("arr[0][0]= " + arr1[0][0]);

		// 2D array with value already inside

		int[][] arr2 = { { 1, 2 }, { 3, 4 } };
		for (int i = 0; i < arr2.length; i++) {
			for (int j = 0; j < arr2.length; j++) {
				System.out.println("arr[" + i + "][" + j + "]= " + arr2[i][j]);

			}
		}

		// Declaring 3D array

		String[][][] threeD_Array = { { { "Apple", "Ball" }, { "Cat", "Dog" } },
				{ { "Egg", "Fish" }, { "Gun", "Hat" } } };
		for (int i = 0; i < threeD_Array.length; i++) {
			for (int j = 0; j < threeD_Array.length; j++) {
				for (int j2 = 0; j2 < threeD_Array.length; j2++) {
					System.out.println("words: arr[" + i + "][" + j + "][" + j2 + "]= " + threeD_Array[i][j][j2]);

				}

			}
			//System.out.println(threeD_Array.length);
		}

	}
}
