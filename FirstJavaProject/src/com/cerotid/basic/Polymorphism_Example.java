package com.cerotid.basic;

//program to show the example of Method Overloading
class MultiplyDemo{
	
	//method with two parameters
	static int Multiply(int a,int b) {
		int c=a*b;
		return c;
	}
	
	//Method with the same name but 2 different values(double) 
	static double Multiply(double a, double b) {
		
		return a*b;
	}
	static int Multiply(int a,int b,int c) {
		
	}
	
	
}

class Main {
	//polymorphism-- many change//forms
	
	public static void main(String[] args) {
		
		//creating an object
		MultiplyDemo multiply =new MultiplyDemo();
		
		//multiply.Multiply(10.00,10.00 );
		//multiply.Multiply(10, 10);
		
		System.out.println(multiply.Multiply(10.00,10.00));
		System.out.println(multiply.Multiply(10, 10));
		
	}

}
