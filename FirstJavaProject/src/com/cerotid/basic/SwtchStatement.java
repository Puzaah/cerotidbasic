package com.cerotid.basic;

public class SwtchStatement {
	public static void main(String[] args) {
		/*switch (key) {case value:
		 * break;
		 * default:break;
		*/
		int day =5;
		switch (day) {
		case 1:
			System.out.println("Its Monday");
			break;
			
		case 2:
			System.out.println("Its Tuesday");
			break;
			
		case 3:
			System.out.println("Its Wednesday");
			break;
			
		case 4:
			System.out.println("Its Thursday");
			break;
			
		case 5:
			System.out.println("Its Friday");
			break;
			
		case 6:
			System.out.println("Its Saturday");
			break;

		
		
		default:
			System.out.println("Please enter a valid week number");
		}

		
		}
		
	}


