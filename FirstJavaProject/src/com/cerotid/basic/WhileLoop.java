package com.cerotid.basic;

public class WhileLoop {
	//main
	 public static void main(String[] args) {
		 //while loop
		 //program to show while loop
		 
		 int x = 1;
		 // exit loop once x >5
		 while(x <=5) {
			 System.out.println("value of x: "+x);
			 
			 //increment value of x till we reach 5
			 //x = x+1;
			 x++;
		 }
		 
	 }

}
