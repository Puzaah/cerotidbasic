package inheritance;

//creating skeleton of this animal
// Parent--default features
public class Animal {
	//fileds/features
	private int numOfLegs;
	private boolean hasEyes;
	private boolean hasTail;
	private String makeNoise;
	private String sex;
	
	
	//default constructor
	public Animal() {
		
	}
	
	
	//overloaded constructor
	public Animal(int numOfLegs, boolean hasEyes, boolean hasTail, String makeNoise, String sex) {
		super();
		this.numOfLegs = numOfLegs;
		this.hasEyes = hasEyes;
		this.hasTail = hasTail;
		this.makeNoise = makeNoise; 
		}
	
	
	//getters/setters
	
	public int getNumOfLegs() {
		return numOfLegs;
	}


	public void setNumOfLegs(int numOfLegs) {
		this.numOfLegs = numOfLegs;
	}


	public boolean HasEyes() {
		return hasEyes;
	}


	public void setHasEyes(boolean hasEyes) {
		this.hasEyes = hasEyes;
	}


	public boolean HasTail() {
		return hasTail;
	}


	public void setHasTail(boolean hasTail) {
		this.hasTail = hasTail;
	}


	public String getMakeNoise() {
		return makeNoise;
	}


	public void setMakeNoise(String makeNoise) {
		this.makeNoise = makeNoise;
	}


	public String getSex() {
		return sex;
	}


	public void setSex(String sex) {
		this.sex = sex;
	}
	
	@Override
	public String toString() {
		return "Animal [numOfLegs=" + numOfLegs + ", hasEyes=" + hasEyes + ", hasTail=" + hasTail + ", makeNoise="
				+ makeNoise + ", sex=" + sex + "]";
	}
}
