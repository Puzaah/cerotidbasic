package inheritance;

//extends--inherits from animal class

public class Cat extends Animal {
	
	//1.fields
	String typeOfCat;
	boolean eatsMeat;
	boolean isFluffy;
	
	//2.constructors
	//default
	public Cat() {
		
	}
	
	//overloaded
	public Cat(String typeOfCat, boolean eatsMeat, boolean isFluffy) {
		super();
		this.typeOfCat = typeOfCat;
		this.eatsMeat = eatsMeat;
		this.isFluffy = isFluffy;
	}
	
	
	//3.getters/setters
	public String getTypeOfCat() {
		return typeOfCat;
	}

	public void setTypeOfCat(String typeOfCat) {
		this.typeOfCat = typeOfCat;
	}

	public boolean isEatsMeat() {
		return eatsMeat;
	}

	public void setEatsMeat(boolean eatsMeat) {
		this.eatsMeat = eatsMeat;
	}

	public boolean isFluffy() {
		return isFluffy;
	}

	public void setFluffy(boolean isFluffy) {
		this.isFluffy = isFluffy;
	}
	
	//4.toString
	@Override
	public String toString() {
		return "Cat [typeOfCat=" + typeOfCat + ", eatsMeat=" + eatsMeat + ", isFluffy=" + isFluffy + "]";
	
	}
	
	
}
