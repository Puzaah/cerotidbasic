package inheritance;

public class TestAnimal {
	public static void main(String[] args) {
		//creating an object 
		//using default constructor as no arguments has been passed
		Animal animal=new Animal();
		
		//utilizing setters
		animal.setHasEyes(true);
		animal.setHasTail(true);
		animal.setMakeNoise("Moo");
		animal.setNumOfLegs(4);
		animal.setSex("Female");
		
		//using getters
		System.out.println(animal.getMakeNoise()+animal.getNumOfLegs()+animal.getSex()+animal.HasEyes()+animal.HasTail());
		
		
		//making another object
		//using overloaded constructor
		Animal animal2 = new Animal(4,true,true,"moo ","male");
		System.out.println(animal2);
	}

}
