
public class Basic1 {

	public static void main(String[] args) {
		System.out.println("Hello World!");
		
		//declaring an int
		int myNumber;
		//giving value to that int
		myNumber = 55;
		//printing out the value
		System.out.println(myNumber);
		
		//8 Primitive Type 
		
		//declaring and giving value in the same time ,them printing it out
		int yourNumber =88;
		//declaring short numbers and long numbers then printing them
		short myShort = 123;
		long myLong = 9797;
		
		double myDouble = 7.345;
		//short version of double is float
		float myFloat = 6.88f;
		
		char myChar ='p';
		boolean myBoolean =true;
		
		//holds upto 8 bit of data
		byte myByte = 127;
		
		System.out.println(yourNumber);
		System.out.println(myShort);
		System.out.println(myLong);
		System.out.println(myDouble);
		System.out.println(myFloat);
		System.out.println(myChar);
		System.out.println(myBoolean);
		System.out.println(myByte);
	}
}

