
public class Basic2 {
	public static void main(String[] args) {
	
   // string is not primitive type. It is a class.String stores sequence of characters.
	//string is a type object. text is type variable. hello is type object
		String text = "Hello";
		String blank = " ";
		String name = "Puja";
		String greeting = text + blank + name;
		System.out.println(greeting);
		//another way
		System.out.println("Hello" + " " + "Puja");
	}

}
