class Machine{
	private String name;
	private int code;
	
	//adding method
	//name of constructor and class should be same
	//constructor doesnt need to have return type
	public Machine() {
		System.out.println("Constructor running");
		
		//setting default value to the constructor
		name= "Arnie";
		
	}
	
	public Machine(String name) {
		System.out.println("second constructor running");
		this.name=name;
	}
	public Machine(String name,int code) {
		System.out.println("Third constructor running");
		this.name=name;
		this.code=code;
	}
	
}

public class Basic6 {
	public static void main(String[] args) {
		//create instance
		Machine machine1= new Machine();
		Machine machine2 = new Machine("Bertie");
		Machine machine3= new Machine("Chalky",7);
		
		//new Machine();
		
	}

}
