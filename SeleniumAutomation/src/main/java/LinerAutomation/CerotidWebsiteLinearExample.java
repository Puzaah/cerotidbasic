package LinerAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsiteLinearExample {
	public static void main(String[] args) throws InterruptedException {
		// step1: Invoke Browser: Navigate to the Cerotid.com page

		// a. set the system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// b.Create a Chromedriver object
		WebDriver driver = new ChromeDriver();

		// c.navigate to cerotid page--two ways to navigate
		// 1.get url driver.get("http://www.cerotid.com")
		// 2. navigate to url
		driver.navigate().to("http://www.cerotid.com");

		Thread.sleep(2000);

		// 3.maximize the window
		driver.manage().window().maximize();

		Thread.sleep(2000);
		// step2: fill form in cerotid page
		// a.created webelement object
		WebElement selectCourse = driver.findElement(By.xpath("//select[@id='classType']"));

		// b. create a select obj and pass the element we want to select
		Select chooseCourse = new Select(selectCourse);

		// c.creating a string variable with coursename
		String courseName = "QA Automation";

		// d.selecting the course by visible text
		chooseCourse.selectByVisibleText(courseName);
 
		// waiting 2 sec
		Thread.sleep(2000);

		// e.selecting the session date
		WebElement selectSessionDate = driver.findElement(By.xpath("//select[@id='sessionType']"));

		// f.creating an obj and passing the element we want to select
		Select chooseDate = new Select(selectSessionDate);

		// g.creating a string variable with selected date
		String choose = "Upcoming Session";

		// h.selecting the date by visible text
		chooseDate.selectByVisibleText(choose);

		Thread.sleep(2000);

		// i.sending full name
		driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Puja Budhathoki");

		Thread.sleep(2000);

		// j.sending address
		driver.findElement(By.xpath("//input[@id='address']")).sendKeys("1234 abcd road blvd");
		driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Irving");

		// k.choosing state
		WebElement chooseState = driver.findElement(By.xpath("//select[@id='state']"));
		Select state = new Select(chooseState);
		String option = "TX";
		state.selectByVisibleText(option);

		// l.sending zip code,email id and phone number
		driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("75039");
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("budhathoki.puzaah@gmail.com");
		driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("1234567890");

		Thread.sleep(1000);

		// m.selecting visa status
		WebElement visaStatus = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		Select status = new Select(visaStatus);
		String option1 = "GreenCard";
		status.selectByVisibleText(option1);

		// n.choosing option for "how did u hear about us?"
		WebElement mediaSource = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		Select source = new Select(mediaSource);
		String option2 = "Friends/Family";
		source.selectByVisibleText(option2);

		// clicking on the No Button
		// create webelement object
		WebElement ableToRelocateNOBtn = driver.findElement(By.xpath("//input[@value='N0']"));
		ableToRelocateNOBtn.click();

		// sending educational background
		driver.findElement(By.xpath("//textarea[@id='eduDetails']")).sendKeys("High School");

		// step 3:validate success message

		// Close will terminate tab
		driver.close();

		// Terminate the browser
		driver.quit();
	}

}
