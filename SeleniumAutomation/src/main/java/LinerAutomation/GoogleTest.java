package LinerAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {
	
	//main method
	public static void main(String[] args) throws InterruptedException {
		

		//1.setting the system path-- proving location of driver
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//2.Creating new Chromedriver object
		
		WebDriver driver = new ChromeDriver();
		 
		//3.invoke the browser/ navigate to google.com
		driver.get("https://www.google.com/");
		
		//4.maximize the browser
		driver.manage().window().maximize();
		
		//5.validate the title is "Google"
		if(driver.getTitle().contains("Google")) {
			System.out.println("Passed-navigated to Valid Page");
		}else {
			System.out.println("Failed- invalid Page");
		}
		
		//perform some action on UI/Webpage
		//Test Case:Navigate tp google.com and search for "what is selenium"
		
		//1.Find the Elements needed to interact with UI for our Test Case
		//note: Two ways to interact
		//a.absolute Xpath-- it contains the complete path from the root element to the desire element
		//b.relative Xpath--This is more like starting simply by referencing the element you want and go from the particular location
		//xpath -- search field//input[@name='q']
		// //--> current node, input-->tag name,  @-->selects attribute, name-->attribute name, q-->value of the attribute
		
		
		//sending keys to search bar
		WebElement txtBoxSearch =driver.findElement(By.xpath("//input[@name='q']"));
		txtBoxSearch.sendKeys("What is selenium");
		
		//pressing enter
		txtBoxSearch.sendKeys(Keys.RETURN);
		
		//to wait--implicit wait
		TimeUnit.SECONDS.sleep(5);
		
		//creating a variable to store title after search
		String titleAfterSearch=driver.getTitle();
		
		//validate after search
		if(titleAfterSearch.contains("What is selenium - Google Search")) {
			System.out.println("Passed-navigated to Valid Page");
		}else {
			System.out.println("Failed- invalid Page");
		}
		
		
		
		//close will terminate tab
		driver.close();
		
		//quit terminate the browser
		driver.quit();
		
	}
	
	
	

}
