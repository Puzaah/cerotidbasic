package LinerAutomation;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class testflightbooking {

	// main method
	public static void main(String[] args) throws InterruptedException {

		// 1.setting the system path-- proving location of driver
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// 2.Creating new Chromedriver object
		WebDriver driver = new ChromeDriver();

		// 3.invoke the browser/ navigate to browser
		driver.get("https://www.flyfrontier.com/");

		// 4.maximize the browser
		driver.manage().window().maximize();

		TimeUnit.SECONDS.sleep(2);

		// 5.declining cookies
		WebElement declineCookies = driver.findElement(By.xpath("//button[@id='onetrust-pc-btn-handler']"));
		declineCookies.click();

		WebElement confirmChoise = driver
				.findElement(By.xpath("//button[@class='save-preference-btn-handler onetrust-close-btn-handler']"));
		confirmChoise.click();

		TimeUnit.SECONDS.sleep(5);

		// 6.filling up the form

		// From
		// a.create webelement object
		WebElement fromOption = driver.findElement(By.xpath("//input[@id='kendoDepartFrom_input']"));
		// b.sending input
		fromOption.sendKeys("DFW");
		TimeUnit.SECONDS.sleep(1);
		// c.pressing enter
		fromOption.sendKeys(Keys.ENTER);

		TimeUnit.SECONDS.sleep(2);
		
		// To
		// a.create webelement object
		WebElement toOption = driver.findElement(By.xpath("//input[@name='kendoArrivalTo_input']"));
		// b.sending input
		toOption.sendKeys("LAX");
		// c.waiting to load
		Thread.sleep(3000);
		// d.pressing enter key
		toOption.sendKeys(Keys.ENTER);

		// Depart Date*
		WebElement datepicker = driver.findElement(By.xpath("//img[@id='departureDateIcon']"));
		datepicker.click();

		TimeUnit.SECONDS.sleep(2);

		// 5.selecting Departure and return date
		WebElement departDate = driver.findElement(By.xpath("//a[@id='7-15-2020']"));
		departDate.click();
		Thread.sleep(5000);
		

		// this will store the current window
		String firstWindow = driver.getWindowHandle();
		// click close when new window opens
		for (String windows : driver.getWindowHandles()) {
			driver.switchTo().window(windows);
		}
		driver.close();
		driver.switchTo().window(firstWindow);

		WebElement returnDate = driver.findElement(By.xpath("//a[@id='7-22-2020']"));
		returnDate.click();

		TimeUnit.SECONDS.sleep(3);

		// Travelers
		// f.creating object and clicking it
		WebElement travelers = driver.findElement(By.xpath("//input[@id='passengersInput']"));
		travelers.click();
		Thread.sleep(3000);
		// click + until we get the desired number
		WebElement addPassenger = driver.findElement(By.xpath("//img[@class='add-adult']"));
		// creating variable to store desired number of passenger
		int numOfPassenger = 2;
		for (int i = 1; i < numOfPassenger; i++) {
			addPassenger.click();
		}

		WebElement closePassengerOption = driver.findElement(By.xpath("//div[@class='invisible-click-handler-input']"));
		closePassengerOption.click();

		// search
		WebElement search = driver.findElement(By.xpath("//img[@src='//F9prodCDN.AzureEdge.Net/images/search_btn.svg']"));
		search.click();

		Thread.sleep(6000);

		// validate departure city name is displayed
		WebElement roundTripFaresTxt = driver.findElement(By.xpath("//div[@class='ibe-roundtrip-header-text']"));

		if (roundTripFaresTxt.getText().contentEquals("ROUND-TRIP FARES")) {
			System.out.println("Passed- Valid Text displayed");
		} else {
			System.out.println("Failed-Expected Text  is not found ");
		}
		
		//validate 
		WebElement underDepartingTxt =driver.findElement(By.xpath("(//div[@class='ibe-p ibe-flightselect-section-header-citypairs'])[2]"));
		if(underDepartingTxt.getText().contentEquals("Dallas/Ft. Worth, TX (DFW) to Los Angeles, CA (LAX)")){
			System.out.println("Passed- Expected text displayed under Departing");
		}else {
			System.out.println("Failed-Expected text is missing");
		}
		driver.close();
		driver.quit();

	}
	

}
