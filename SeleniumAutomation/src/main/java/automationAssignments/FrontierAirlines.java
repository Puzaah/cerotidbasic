package automationAssignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrontierAirlines {

	// creating class level variable
	static WebDriver driver;

	// main method
	public static void main(String[] args) throws InterruptedException {

		// 1.invoke the browser
		invokeBrowser();

		// 2.decline Cookies
		declineCookies();

		// 3.fill the form
		fillForm();
		
		// 4. validate page
		validate();
		
		// 5.terminate the Browser
		terminateBrowser();
	}
	
	private static void terminateBrowser() {
		
		// 1. terminate tab
		driver.close();
		
		// 2.terminate the browser
		driver.quit();
		
	}

	private static void validate() throws InterruptedException {
		
		// 1.validating title of page after search
		if (driver.getTitle().contentEquals("Select | Frontier Airlines")) {
					System.out.println("Valid page found");
				}
		
		//wait to load
		Thread.sleep(6000);
		
		// 3.validate departure city name is displayed
		WebElement roundTripFaresTxt = driver.findElement(By.xpath("//div[@class='ibe-roundtrip-header-text']"));

		if (roundTripFaresTxt.getText().contentEquals("ROUND-TRIP FARES")) {
			System.out.println("Passed- Valid Text displayed");
		} else {
			System.out.println("Failed-Expected Text  is not found ");
		}
		
		// 4.validate 
		WebElement underDepartingTxt =driver.findElement(By.xpath("(//div[@class='ibe-p ibe-flightselect-section-header-citypairs'])[2]"));
		
		if(underDepartingTxt.getText().contentEquals("Dallas/Ft. Worth, TX (DFW) to Los Angeles, CA (LAX)")){
			System.out.println("Passed- Expected text displayed under Departing");
		}else {
			System.out.println("Failed-Expected text is missing");
		}
		
	}

	public static void fillForm() throws InterruptedException {

		try {
			// 1.From
			// a.create webelement object
			WebElement fromOption = driver.findElement(By.xpath("//input[@id='kendoDepartFrom_input']"));

			// b.type input
			fromOption.sendKeys("DFW");

			// c.wait to load
			TimeUnit.SECONDS.sleep(1);

			// d.press enter
			fromOption.sendKeys(Keys.ENTER);

			// e.wait
			TimeUnit.SECONDS.sleep(2);

			
			// 2.To
			// a.create webelement object
			WebElement toOption = driver.findElement(By.xpath("//input[@name='kendoArrivalTo_input']"));

			// b.type input
			toOption.sendKeys("LAX");

			// c.waiting to load
			Thread.sleep(3000);

			// d.press enter key
			toOption.sendKeys(Keys.ENTER);
			
			
			// 3.Calender 
			// a.create webelement object and click on calender icon
			WebElement calenderIcon=driver.findElement(By.xpath("//img[@id='departureDateIcon']"));
			calenderIcon.click();
			
			// b.wait to load
			TimeUnit.SECONDS.sleep(2);
			
			// c.selecting Departure date
			WebElement departDate=driver.findElement(By.xpath("//a[@id='7-15-2020']"));
			departDate.click();
			
			// d.wait to load
			Thread.sleep(5000);
			
			// e.new window
			// e.1.store current window
			String firstWindow = driver.getWindowHandle();
			
			// e.2.click close when new window opens
			for (String windows : driver.getWindowHandles()) {
				driver.switchTo().window(windows);
			}
			driver.close();
			// e.3.switch to previous window
			driver.switchTo().window(firstWindow);
			
			// f.WebElement returnDate = driver.findElement(By.xpath("//a[@id='7-22-2020']"));
			WebElement returnDate=driver.findElement(By.xpath("//a[@id='7-22-2020']"));
			returnDate.click();
			
			// g.wait to load
			TimeUnit.SECONDS.sleep(3);
			
			// 4.to scroll down the screen
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,300)");
			
			// 5.Travelers
			// a.create webelement object and click it
			WebElement travelers = driver.findElement(By.xpath("//input[@id='passengersInput']"));
			travelers.click();
			
			// b.wait to load
			Thread.sleep(3000);
			
			// c.adding adults
			// c.1.create webelement object
			WebElement addAdults = driver.findElement(By.xpath("//img[@class='add-adult']"));
			
			// c.2.creating variable to store desired number of passenger
			int numOfPassenger = 2;
			
			// c.3.create webelement object of displayed value
			WebElement displayedNum=driver.findElement(By.xpath("//p[@id='adult-count']"));
			
			// c.4.convert text(displayed value) to int and create variable to store it
			int currentSelect = Integer.parseInt(displayedNum.getText());
			
			// c.5.click "+" until we get the desired number
			while(currentSelect<numOfPassenger) {
				addAdults.click();
				displayedNum=driver.findElement(By.xpath("//p[@id='adult-count']"));
				currentSelect = Integer.parseInt(displayedNum.getText());
			}
			
			// c.6.click and close the dropdown
			WebElement closePassengerOption = driver.findElement(By.xpath("//div[@class='invisible-click-handler-input']"));
			closePassengerOption.click();
			
			// 7.search
			// 7.a.create webelement object for search and click
			WebElement search = driver.findElement(By.xpath("//img[@src='//F9prodCDN.AzureEdge.Net/images/search_btn.svg']"));
			search.click();
			
			// 7.b.wait to load the page
			Thread.sleep(6000);


		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

	}

	private static void declineCookies() throws InterruptedException {

		// 1.create webelement object and decline cookies
		WebElement declineCookies = driver.findElement(By.xpath("//button[@id='onetrust-pc-btn-handler']"));
		declineCookies.click();
		
		//2.create webelement to confirm decline
		WebElement confirmChoice = driver.findElement(By.xpath("//button[@class='save-preference-btn-handler onetrust-close-btn-handler']"));
		confirmChoice.click();

		// 7.wait to load
		TimeUnit.SECONDS.sleep(2);

	}
	
	public static void invokeBrowser() throws InterruptedException {

		// 1.setting the system path-- proving location of driver
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// 2.Creating new Chromedriver object
		driver = new ChromeDriver();

		// 3.invoke the browser/ navigate to browser
		driver.get("https://www.flyfrontier.com/");

		// 4.maximize the browser
		driver.manage().window().maximize();

		// 5.wait time to maximize the window
		TimeUnit.SECONDS.sleep(2);

	}

}
